
# Layui-doc

#### 介绍
    1、新增模板商城源代码
    2、修复原有bug

### QQ群
  <a target="_blank" href="https://qm.qq.com/cgi-bin/qm/qr?k=Ud9zOTKW3DDx959_oigK3mdHQQ1Xa4OF&jump_from=webapi"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="Layui+" title="Layui+"></a>

[layui-electron](https://gitee.com/layuiplus/layui-electron) 可生成用layui开发桌面软件

## 怀念陪伴一起渡过的时光
2021 年 9 月 24 日 发布官网停止维护后，第一时间整站文档备份，后续会整合一套layui+vue的jquery框架，后续开源.





